"""
Setup for apk_launch project
"""

import distutils.cmd
import distutils.log
import sys
import os
import setuptools
from glob import glob

VERSION = '0.2'

from setuptools import setup, find_packages
from setuptools.command.install import install
from setuptools.extension import Extension
from setuptools.command.test import test as TestCommand
import subprocess

requirements = ['psutil', 'kafka-python', 'couchbase']

def get_virtualenv_path():
    """Used to work out path to install compiled binaries to."""
    if hasattr(sys, 'real_prefix'):
        return sys.prefix

    if hasattr(sys, 'base_prefix') and sys.base_prefix != sys.prefix:
        return sys.prefix

    if 'conda' in sys.prefix:
        return sys.prefix

    return None


setuptools.setup(
    name='termwatchserver',
    packages=setuptools.find_packages(exclude=('tests', 'docs')),
    license=open('LICENSE.txt').read(),
    include_package_data=True,
    version=VERSION,
    description='Tool for monitoring long running terminal tasks',
    long_description=open('README.md').read(),
    author='Zach Yannes',
    author_email='zachyannes@gmail.com',
    url='https://github.com/Zachout',
    # download_url='https://github.com/Zachout/dex2proto/archive/0.1.tar.gz',
    keywords=['terminal', 'watch', 'notification', 'server', 'kafka', 'couchbase'],
    classifiers=[],
    setup_requires=['pytest-runner', 'pytest-pylint'],
    tests_require=['pytest', 'pylint'],
    install_requires=requirements,
    scripts=['scripts/termwatchserver'],
    # ext_modules = [termwatch_module],
)
