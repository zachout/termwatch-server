/* Copyright 2002 by Jeff Chang.  All rights reserved.
 * Revisions copyright 2006, 2010 by Michiel de Hoon.  All rights reserved.
 *
 * This file is part of the Biopython distribution and governed by your
 * choice of the "Biopython License Agreement" or the "BSD 3-Clause License".
 * Please see the LICENSE file that should have been included as part of this
 * package.
 *
 * trie.h
 *
 * Used by trie.c, triemodule.c and thus also findtrie.py
 */

 #include <sys/wait.h>
 #include <sys/resource.h>
 #include <stdio.h>
 #include <signal.h>
 #include <getopt.h>
 #include <inttypes.h>
 #include <errno.h>
 #include <stdlib.h>
 #include <stdbool.h>
 #include <stdnoreturn.h>
 #include <string.h>
 #include <limits.h>
 #include <unistd.h>
 #include <Python.h>

#define SIGNALLED_OFFSET 128

PyObject *pFunc;
PyObject *pProcOutput, *pInitProducer, *pPublishMessage, *pCloseProducer;

void setup_python_function();
void call_watch_process(int);
void python_finalize();

void start_process(void);
