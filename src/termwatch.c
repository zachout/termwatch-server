/* Copyright 2002-2003, 2013 by Jeff Chang.  All rights reserved.
 * Revisions copyright 2006, 2010 by Michiel de Hoon.  All rights reserved.
 * Revisions copyright 2017 by Oscar G. Garcia.  All rights reserved.
 *
 * This file is part of the Biopython distribution and governed by your
 * choice of the "Biopython License Agreement" or the "BSD 3-Clause License".
 * Please see the LICENSE file that should have been included as part of this
 * package.
 *
 * trie.c
 *
 * Used by triemodule.c and thus also findtrie.py
 */
#include "termwatch.h"
#define BUFLEN 2048
#define MAXURLLEN 128
#define MAXURLS 16

#define FINISHED 0
#define RUNNING 1
#define FAILED -1

struct MyOptions {
  char *url;
  int verbose;
  char cmd[BUFLEN];
};

struct MyOptions gOptions;

noreturn static void
usage ()
{
  printf ("Usage: termwatch [-h] [-v] [-u kafka url ...] command [arg...]\n");
  exit (EXIT_SUCCESS);
}

void setup_python_functions() {
  PyObject *sys, *path, *curDir, *pName, *pModule, *pDict;
  Py_Initialize();
  sys  = PyImport_ImportModule("sys");
  path = PyObject_GetAttrString(sys, "path");
  curDir = PyString_FromString(".");
  PyList_Append(path, curDir);
  Py_XDECREF(curDir);
  Py_XDECREF(path);
  Py_XDECREF(sys);

  // watch_process
  pName = PyString_FromString((char*)"termwatch.utils.proc_utils");
  pModule = PyImport_Import(pName);
  if(!pModule) {
    PyErr_Print();
    printf("ERROR in pModule\n");
    exit(1);
  }
  Py_XDECREF(pName);
  pDict = PyModule_GetDict(pModule);

  pFunc = PyDict_GetItemString(pDict, "watch_process");
  if(!pFunc || !PyCallable_Check(pFunc)) {
    PyErr_Print();
  }

  pPublishMessage = PyDict_GetItemString(pDict, "proc_output");
  if(!pPublishMessage || !PyCallable_Check(pPublishMessage)) {
    PyErr_Print();
  }

  // watch_process
  pName = PyString_FromString((char*)"termwatch.utils.kafka_utils");
  pModule = PyImport_Import(pName);
  if(!pModule) {
    PyErr_Print();
    printf("ERROR in pModule\n");
    exit(1);
  }
  Py_XDECREF(pName);
  pDict = PyModule_GetDict(pModule);

  pInitProducer = PyDict_GetItemString(pDict, "init_couch_bucket");
  if(!pInitProducer || !PyCallable_Check(pInitProducer)) {
    PyErr_Print();
  }

}

void call_watch_process(int pid) {
  PyObject* args = Py_BuildValue("(i)", pid);
  if(!args) {
    PyErr_Print();
    return;
  }

  // PyGILState_STATE gstate;
  // gstate = PyGILState_Ensure();
  // if (! PyEval_ThreadsInitialized()) {
  //   PyEval_InitThreads();
  // }

  PyObject* pResult = PyObject_CallObject(pFunc, args);
  // PyGILState_Release(gstate);
}

void call_init_couch_bucket(char *serverUrl) {
  int i;
  PyObject *args;
  args = Py_BuildValue("(s)", serverUrl);
  PyObject* pResult = PyObject_CallObject(pInitProducer, args);
}

void call_publish_topic_message(int pid, char* output, int status) {
  PyObject* args = Py_BuildValue("(isi)", pid, output, status);
  if(!args) {
    PyErr_Print();
    return;
  }

  // PyGILState_STATE gstate;
  // gstate = PyGILState_Ensure();
  // if (! PyEval_ThreadsInitialized()) {
  //   PyEval_InitThreads();
  // }

  PyObject* pResult = PyObject_CallObject(pPublishMessage, args);
  // PyGILState_Release(gstate);
}

void call_close_kafka_producer() {
  PyObject* pResult = PyObject_CallObject(pCloseProducer, NULL);
}

void python_finalize() {
  if(pFunc) {
    Py_XDECREF(pFunc);
  }
  if(pInitProducer) {
    Py_XDECREF(pInitProducer);
  }
  if(pPublishMessage) {
    Py_XDECREF(pPublishMessage);
  }
  if(pCloseProducer) {
    Py_XDECREF(pCloseProducer);
  }
  Py_Finalize();
}

static void
run_command (cmd, res)
     const char *cmd;
     int* res;
{
  __sighandler_t interrupt_signal, quit_signal;
  int saved_errno, status, fd[2];
  pid_t pid;
  FILE *cmd_output;
  char buf[BUFLEN];

  if(pipe(fd)) {
    fprintf(stderr, "Error creating pipe\n");
    exit(ENOENT);
  }

  if ((pid = fork()) == -1) {
    exit(EXIT_FAILURE);
  } else if(pid == 0) {
    dup2(fd[1], STDOUT_FILENO);
    close(fd[0]);
    close(fd[1]);
    execl("/bin/sh", "sh", "-c", cmd, (char *)NULL);
    saved_errno = errno;
    fprintf(stderr, "cannot run %s", cmd);
    _exit (ENOENT);
  } else {
    close(fd[1]);

    call_init_couch_bucket(gOptions.url);
    fprintf(stderr, "Initialized couchbase bucket\n");

    cmd_output = fdopen(fd[0], "r");

    while (fgets(buf, sizeof buf, cmd_output)) {
      fprintf(stderr, "Output: %s\n", buf);
      call_publish_topic_message(pid, buf, RUNNING);
    }

    wait(res);
    call_publish_topic_message(pid, (char*) NULL, FINISHED);
  }

  interrupt_signal = signal (SIGINT, SIG_IGN);
  quit_signal = signal (SIGQUIT, SIG_IGN);

  signal (SIGINT, interrupt_signal);
  signal (SIGQUIT, quit_signal);
}

int
main (argc, argv)
     int argc;
     char **argv;
{
  int i, status, waitstatus;
  // getargs(argc, argv);

  if(argc == 1) {
    usage();
  }

  gOptions.url = (char*) malloc(MAXURLLEN);
    strcpy(gOptions.url, "10.0.0.155");

  sprintf(gOptions.cmd, "%s", argv[1]);
  for(i = 2; i < argc; i++) {
    sprintf(gOptions.cmd, "%s %s", gOptions.cmd, argv[i]);
  }

  // set_program_name (argv[0]);
  setup_python_functions();

  run_command ((const char *) gOptions.cmd, &waitstatus);
  fprintf(stdout, "Command result: %d\n", waitstatus);

  if (WIFSTOPPED (waitstatus))
    status = WSTOPSIG (waitstatus) + SIGNALLED_OFFSET;
  else if (WIFSIGNALED (waitstatus))
    status = WTERMSIG (waitstatus) + SIGNALLED_OFFSET;
  else if (WIFEXITED (waitstatus))
    status = WEXITSTATUS (waitstatus);
  else {
    fprintf(stderr, "unknown status from command: %d\n", waitstatus);
    status = EXIT_FAILURE;
  }

  // for(i = 0; i < argc - 1; i++) {
  //   free(cmd[i]);
  //   free(cmd);
  // }

  python_finalize();

  return status;
}
