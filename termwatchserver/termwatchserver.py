"""
    Run termwatchserver
"""
import argparse
import signal
import sys
import os
import datetime
import logging

import constants
from .utils import proc_utils
from .utils import kafka_couchbase_utils

logging.basicConfig(level=logging.INFO, format='%(message)s')
# logging.basicConfig(filename=constants.LOGFILE, level=logging.INFO)
LOG = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser(
        prog='PROG',
        description='Run termwatchserver',
    )

    parser.add_argument('-v', '--verbose',
                        help='Prints command output to screen',
                        action='store_true',
                        default=False)

    parser.add_argument('-d', '--daemon',
                        help='Start termwatchserver as daemon',
                        action='store_true')

    parser.add_argument('-ku', '--kafkaurl',
                        help='Url of kafka server',
                        action='append')

    parser.add_argument('-cu', '--couchbaseurl',
                        help='Url of couchbase server',
                        type=str, default='10.0.0.155')

    parser.add_argument('-u', '--user',
                        help='Couchbase user',
                        type=str)

    parser.add_argument('-p', '--passwd',
                        help='Couchbase passwd',
                        type=str)

    args, _ = parser.parse_known_args()
    args = vars(args)

    if args['verbose']:
        LOG.setLevel(logging.DEBUG)
        logging.getLogger('termwatchserver.utils.kafka_couchbase_utils').setLevel(logging.DEBUG)
    else:
        LOG.setLevel(logging.INFO)
        logging.getLogger('termwatchserver.utils.kafka_couchbase_utils').setLevel(logging.INFO)

    LOG.debug('Args: %s', args)

    if args['dryrun']:
        sys.exit(0)
    return args


def signal_handler(signum, frame):
    LOG.info('Received signal (%d): frame %s', signum, frame)
    os.killpg(0, signal.SIGKILL)
    sys.exit(signum)


def main():
    args = parse_args()
    signal.signal(signal.SIGINT, signal_handler)
    os.setpgrp()

    if not args['kafkaurl']:
        args['kafkaurl'] = ['10.0.0.155:9092']

    consumer = kafka_couchbase_utils.Consumer(args['kafkaurl'], ['termwatch'], args['couchbaseurl'], args['user'], args['passwd'])
    consumer.start()
    consumer.join()

    return 0
