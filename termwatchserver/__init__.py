__all__ = ('HOMEPATH', 'PLATFORM', '__version__')

import os
import sys

import re
import argparse
import signal

__version__ = '1.0'

PACKAGEPATH = os.path.abspath(os.path.dirname(__file__))

HOMEPATH = os.path.dirname(PACKAGEPATH)
