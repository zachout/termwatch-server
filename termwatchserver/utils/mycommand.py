"""
    Run command and print lines to log file.
"""
import os, sys
import subprocess
import argparse
import datetime
import logging


logging.basicConfig(level=logging.INFO, format='%(message)s')
LOG = logging.getLogger(__name__)


class Command(object):
    def __init__(self, cmd, outfile=None, verbose=False, handler=None):
        self.cmd = cmd
        self.outfile = outfile
        self.filehandler = None if not outfile else open(outfile, 'w')
        self.handler = handler
        self.process = None
        self.log = []


    def __del__(self):
        if self.filehandler:
            self.filehandler.close()


    def run(self, data):
        self.process = subprocess.Popen(self.cmd, stdout=subprocess.PIPE,
                                        shell=True, universal_newlines=True)
        self.pid = self.process.pid
        if not self.outfile:
            self.outfile = '/tmp/{}.log'.format(self.pid)
            LOG.debug('Logging command to %s', self.outfile)
            self.filehandler = open(self.outfile, 'w')
        LOG.debug('Running cmd: %s', self.cmd)
        data['logFile'] = self.outfile
        data['command'] = self.cmd
        data['pid'] = self.pid
        data['startTime'] = unicode(datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S'))
        if self.handler:
            data['data'] = None
            data['status'] = 'started'
            self.filehandler.flush()
            self.handler(data)

        try:
            for line in iter(self.process.stdout.readline, ''):
                self.log.append(line)
                self.filehandler.write(line)
                if self.handler:
                    data['data'] = line
                    data['status'] = 'running'
                    self.filehandler.flush()
                    self.handler(data)
            self.process.wait()
        except KeyboardInterrupt as e:
            LOG.error('Killed process %d', self.pid)
            LOG.error(e)
            self.filehandler.write('Killed process {}'.format(self.pid))
            if self.handler:
                data['data'] = self.log
                data['status'] = 'killed'
                self.filehandler.flush()
                self.handler(data)
        if self.handler:
            data['data'] = self.log
            data['status'] = 'completed'
            self.filehandler.flush()
            self.handler(data)
        return self.log

    def get_log_file(self):
        return self.outfile

    def get_log(self):
        return self.log


def simple_handler(outfile, status, line):
    LOG.debug('[%s] %s: "%s"', outfile, status, line)


def parse_args():
    parser = argparse.ArgumentParser(
        prog='PROG',
        description='Run terminal command and log to firebase',
    )

    parser.add_argument('command',
                        help='Command to watch',
                        nargs='+')


    parser.add_argument('-v', '--verbose',
                        help='Print verbose logging',
                        action='store_true')

    args, _ = parser.parse_known_args()
    args = vars(args)

    if args['verbose']:
        LOG.setLevel(logging.DEBUG)
    else:
        LOG.setLevel(logging.INFO)

    LOG.debug('Args: %s', args)
    return args

def main():
    args = parse_args()
    LOG.info('mycommand: %s', args)
    cmd = Command(' '.join(args['command']), handler=simple_handler)
    cmd.run()
