#!/usr/bin/env python
import os, sys
import re
import argparse

import socket
import signal
import psutil
import time
import datetime
import threading
import logging
import json
from kafka import KafkaConsumer

logging.basicConfig(level=logging.INFO, format = '%(message)s')
LOG = logging.getLogger(__name__)


class Consumer(threading.Thread):
    daemon = True

    def __init__(self, kafkaservers, topics, couchbaseserver, user, passwd):
        self.kafkaservers = kafkaservers
        self.topics = topics
        self.couchbaseserver = couchbaseserver
        self.cluster = self.init_couchbase(self.couchbaseserver, user, passwd)


    def init_couchbase(self, server, user, passwd):
        cluster = Cluster('couchbase://' + server)
        authenticator = PasswordAuthenticator(user, passwd)
        return cluster


    def run(self):
        consumer = KafkaConsumer(bootstrap_servers=self.servers,
                                 auto_offset_reset='earliest',
                                 value_deserializer=lambda m: json.loads(m.decode('utf-8')))
        consumer.subscribe(self.topics)

        for message in consumer:
            LOG.info(message)
            # TODO: append to couchbase
            update_couchbase_item(message.topic, message.key, message.value)


    def update_couchbase_item(self, topic, key, value):
        LOG.info('Topic %s = %s', key, value)
        bucket = self.cluster.open_bucket(topic)
        oldval = bucket.get(key)
        value['output'] = oldval['output'] + value['output']
        bucket.upsert(key, value)
