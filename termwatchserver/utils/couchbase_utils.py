#!/usr/bin/env python
import os, sys
import re
import argparse

import socket
import signal
import psutil
import time
import datetime
from threading import Thread
import logging

from couchbase.cluster import Cluster
from couchbase.cluster import PasswordAuthenticator

logging.basicConfig(level=logging.INFO, format = '%(message)s')
LOG = logging.getLogger(__name__)

bucket = None

def init_couch_bucket(server=None):
    global bucket
    if not server:
        server='10.0.0.155' # :11210'

    cluster = Cluster('couchbase://' + server)
    authenticator = PasswordAuthenticator('yannes', 'ClusterIHardlyKnewHer')
    cluster.authenticate(authenticator)
    bucket = cluster.open_bucket('termwatch')
    return bucket


def publish_message(topic, key, value):
    LOG.info('ID %s: %s = %s', topic, key, value)
    try:
        bucket.upsert(topic, key, value)
        producer.flush()
        LOG.info('Message published successfully.')
    except Exception as ex:
        LOG.error('Could not publish message: %s', ex)
