#!/usr/bin/env python
import os, sys
import re
import argparse

import socket
import signal
import psutil
import time
import datetime
from threading import Thread
import logging

from . import kafka_utils
from . import couchbase_utils

logging.basicConfig(level=logging.INFO, format = '%(message)s')
LOG = logging.getLogger(__name__)

SLEEP_INTERVAL = 10

FINISHED = 0
RUNNING = 1
FAILED = -1

def get_proc_date(proc):
    fulldate = '{} {}'.format(proc['create_date'], proc['create_time'])
    return time.strptime(fulldate, '%Y-%m-%d %H:%M:%S')


def process_finished(procdict, status='finished'):
    procdict['status'] = status # 'finished'
    procdict['finish_time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return procdict


def watch_process_thread(pid, interval):
    while is_running(pid):
        LOG.info('Process %d is still running', pid)
        time.sleep(interval)


def watch_process(pid, interval=SLEEP_INTERVAL):
    data = get_process_data(pid)
    LOG.info('Watching process %s', data)
    while pid_exists(pid):
        LOG.info('Process %d is still running', pid)
        time.sleep(SLEEP_INTERVAL)
    return data


def proc_output(pid, output, status=RUNNING):
    LOG.info('Process %d: "%s"', pid, output)
    # Send output to kafka stream (topic = hostname)
    data = {
        'pid': pid,
        'hostname': socket.gethostname(),
        'output': output,
        'status': RUNNING if pid_exists else FINISHED
    }
    kafka_utils.publish_topic_message(socket.gethostname(), str(pid), str(data))
    # couchbase_utils.publish_message(socket.gethostname(), str(pid), str(data))



def pid_exists(pid):
    """Check whether pid exists in the current process table."""
    proc = psutil.Process(pid)
    return proc.status() != psutil.STATUS_ZOMBIE

def is_running(pid):
    return os.path.exists('/proc/{}'.format(pid))

def get_process_data(pid):
    info = []
    try:
        proc = psutil.Process(pid)

        data = proc.as_dict(['pid', 'create_time', 'status', 'cmdline'])
        local = time.localtime(data['create_time'])
        data['hostname'] = socket.gethostname()
        data['create_time'] = time.strftime('%Y-%m-%d %H:%M:%S', local)
        data['cmdline'] = ' '.join(data['cmdline'])
    except psutil.NoSuchProcess as e:
        LOG.error('Error: %s', e)
        data = {'pid': pid, 'status': 'finished'}

    LOG.debug(data)
    return data
