"""
    Perform utility firebase operations.
"""
import json
import datetime
import time
import socket
import logging
import argparse
import mimetypes
import httplib2
import firebase_admin
import google
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import storage
from oauth2client.service_account import ServiceAccountCredentials

import constants

# logging.basicConfig(filename=constants.LOGFILE, level=logging.INFO)
logging.basicConfig(level=logging.INFO, format='%(message)s')
LOG = logging.getLogger(__name__)


def get_project_url(googlefile):
    """
    Read firebase_url from google-services.json file.

    """
    with open(googlefile, 'r') as rfh:
        data = json.loads(rfh.read())

    return data['project_info']['firebase_url']


def get_storage_bucket(googlefile):
    """
    Read storage_bucket from google-services.json file.

    """
    with open(googlefile, 'r') as rfh:
        data = json.loads(rfh.read())

    return data['project_info']['storage_bucket']


def login(certificatefile, googlefile):
    """
    Login to firebase project using <project>-firebase.json file.

    """
    firebaseurl = get_project_url(googlefile)
    storagebucket = get_storage_bucket(googlefile)
    LOG.debug('Logging in to firebase %s', firebaseurl)
    cred = credentials.Certificate(certificatefile)
    try:
        firebase_admin.initialize_app(cred, options={
            'databaseURL': firebaseurl,
            'storageBucket': storagebucket,
        })
    except ValueError:
        LOG.error('Already logged in')
    return cred


def refresh_token(cred):
    cred.refresh(httplib2.Http())
    return cred


def save_to_firestore(key, values, certificatefile, googlefile):
    fileid = '{}.{}'.format(socket.gethostname(), key.split('/')[-1])
    fileUrl = upload_to_storage(key)
    client = firestore.client()
    doc_ref = client.collection('processes').document(fileid)

    data = values
    data[u'fileName'] = unicode(fileid)
    data[u'timeStamp'] = unicode(datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S'))
    data[u'fileUrl'] = unicode(fileUrl)
    data[u'hostName'] = unicode(socket.gethostname())

    LOG.debug('Updating file %s', fileid)
    LOG.debug('Updating db: %s = %s', fileid, data)
    try:
        res = doc_ref.set(data)
    except google.api_core.exceptions.ServiceUnavailable:
        login(certificatefile, googlefile)
        res = doc_ref.set(data)
    return res

def upload_to_storage(filename):
    """
    Write data to firestore 'files' collection under document 'filename' and
        save file data to storage.

    """
    fileid = '{}.{}'.format(socket.gethostname(), filename.split('/')[-1])
    try:
        with open(filename, 'rb') as rfh:
            file_data = rfh.read()
    except IOError:
        return None

    LOG.debug('Uploading %s to %s (%d bytes)', filename, fileid, len(file_data))
    bucket = storage.bucket()
    blob = bucket.blob(fileid)
    filetype, _ = mimetypes.guess_type(filename)
    blob.upload_from_string(file_data, content_type=filetype)
    return blob.public_url
